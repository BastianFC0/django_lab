from django.contrib import admin
from . models import *
all_models = (Question, Choice)
# Register your models here.
admin.site.register(all_models)