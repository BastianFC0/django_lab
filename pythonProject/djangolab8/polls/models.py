from django.db import models

# Create your models here.
class Question (models.Model):
    quest_text = models.CharField(max_length=200, help_text="enter a question...")
    pub_date = models.DateTimeField(auto_now_add = True)

    def __str__(self):
        return f"{self.quest_text[:10]}..."

class Choice (models.Model):
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    choice_text = models.CharField(max_length=180, help_text="enter a choice")
    vote = models.IntegerField(default = 0)

    def __str__(self):
        return f"{self.choice_text[:10]}-->{self.question.quest_text}[:10]"