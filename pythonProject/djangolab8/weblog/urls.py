from django.urls import path
from . import views

urlpatterns = [
    path("contact/", views.MyContactTPView.as_view(), name = "contact"),
    path('home', views.MyHomeView.as_view(), name = "home"),
    path('', views.MyHomeView.as_view(), name = "home")
]