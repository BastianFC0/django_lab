from django.contrib import admin
from . models import *
all_models = (Author, Post)
# Register your models here.
admin.site.register(all_models)