from django.db import models

# Create your models here.
class Author(models.Model):
    name = models.CharField(max_length=200, help_text="enter author name")
    age = models.IntegerField(default=18, help_text="enter an age >=18")
    email =  models.EmailField(help_text="enter email")

    def __str__(self):
        return self.email

class Post(models.Model):
    owner = models.ForeignKey(Author, on_delete=models.CASCADE)
    title =  models.CharField(max_length=200, help_text="enter post title")
    body = models.TextField(help_text="enter the content of the post")
    pub_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"{self.title[:15]}"