from django.shortcuts import render

# Create your views here.

#def home(req):
    #return render(req, "weblog/home.html")

def contact(req):
    return render(req, "weblog/contact.html")

from django.views.generic import View, TemplateView, ListView, DetailView
class MyHomeView(View):
    template_name = "weblog/home.html"
    def get(self, req, *args, **kwargs):
        data = {"page_title": "Home page",
        "greet": "Welcome to the Home Page"}
        return render(req, self.template_name, context = data)


class MyContactTPView(TemplateView):
    template_name = "weblog/contact.html"
    def get_context_data(self, **kwargs):
        context_data = super().get_context_data(**kwargs)
        #context_data["page_title"] = "TP Contact Page"
        return context_data

#This can be two lines if you follow specific rules for the name of the list
#and html template
#class MyListPosts(ListView):
 #   model = Post
  #  template_name = "weblog/post_list.html"
   # context_object_name = "list_posts"

#DetailView is only used to display a single instance of an item in a list
#This is the Views version, the DetailView is the next one
#class MyPostDetailView(View):
    #model = Post
    #template_name = "weblog/my_detail_post.html"
    #def get(self, req, *args, **kwargs):
        #my_post = get_object_or_404(Post, id = kwargs["id"])
        #return render(req, self.template_name, {"p": my_post})

#Like ListView, the code can be short like this if we follow some naming rules
#namely renaming the templates to <something>_detail.html and the list
#object in it as object_list, and objects as object
#class MyPostDetail(DetailView):
    #model = Post